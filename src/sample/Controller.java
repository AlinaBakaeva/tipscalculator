package sample;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Приложение для расчета чаевых
 *
 * @Author Bakaeva.A.-18IT18
 */
public class Controller {

    @FXML
    private ComboBox<String> currencyComboBox;

    @FXML
    private TextField inputBill;

    @FXML
    private RadioButton noTips;

    @FXML
    private RadioButton tips5Percent;

    @FXML
    private RadioButton tips10Percent;

    @FXML
    private CheckBox round;

    @FXML
    private CheckBox outputInRub;

    @FXML
    private Label tipsRubLabel;

    @FXML
    private Label totalRubLabel;

    @FXML
    private TextArea tipsRubOutput;

    @FXML
    private TextArea totalRubOutput;

    @FXML
    private TextArea totalOutput;

    @FXML
    private TextArea tipsOutput;

    @FXML
    private Label selectTipsWarning;

    private double tips;
    private double total;
    private double tipsInRub;
    private double totalInRub;
    private BigDecimal[] sumsDecimal;

    @FXML
    void startApp() {
        if (!isCorrectData()) outputWarning();
        else if (!isSelectedCurrency()) currencyComboBoxWarning();
        else if (!isSelectedPercentTips()) tipsRadioButtonsWarning();
        else {
            inputBill.setPromptText("Введите сумму");
            inputBill.setStyle("-fx-background-color: #FFFFFF;");
            selectTipsWarning.setVisible(false);
            calculate();
        }
    }

    /**
     * Метод рассчитывает чаевые
     */
    @FXML
    private void calculate() {
        double bill = Double.parseDouble(inputBill.getText());
        double tipsPercent = getTips();
        tips = bill * tipsPercent;
        total = bill + tips;
        calcInOtherCurrencies();
        if (round.isSelected()) {
            roundSums();
        }
        setBinaryFormat();
        outputsSums();
    }

    /**
     * Округляет суммы до целого числа
     */
    private void roundSums() {
        tips = Math.round(tips);
        total = Math.round(total);
        tipsInRub = Math.round(tipsInRub);
        totalInRub = Math.round(totalInRub);
        setBinaryFormat();
    }

    /**
     * Считает чаевые в другой валюте
     */
    private void calcInOtherCurrencies() {
        if (currencyComboBox.getSelectionModel().getSelectedIndex() == 0) {
            tipsInRub = tips * 70.21;
            totalInRub = total * 70.21;
        }
        if (currencyComboBox.getSelectionModel().getSelectedIndex() == 1) {
            tipsInRub = tips * 79.21;
            totalInRub = total * 79.21;
        }
    }

    /**
     * Задает двоичный формат числам
     */
    private void setBinaryFormat() {
        sumsDecimal = new BigDecimal[4];
        double[] tempValues = {tips, tipsInRub, total, totalInRub};
        for (int i = 0; i < sumsDecimal.length; i++) {
            sumsDecimal[i] = BigDecimal.valueOf(tempValues[i]);
            sumsDecimal[i] = sumsDecimal[i].setScale(2, RoundingMode.CEILING);
        }
    }

    /**
     * Выводит суммы в поля вывода
     */
    private void outputsSums() {
        tipsOutput.setText(String.valueOf(sumsDecimal[0]));
        tipsRubOutput.setText(String.valueOf(sumsDecimal[1]));
        totalOutput.setText(String.valueOf(sumsDecimal[2]));
        totalRubOutput.setText(String.valueOf(sumsDecimal[3]));
    }

    /**
     * Метод для получения процента чаевых
     *
     * @return процент
     */
    private double getTips() {
        if (noTips.isSelected()) return 0;
        if (tips5Percent.isSelected()) return 0.05;
        return 0.1;
    }

    /**
     * Выводит предупреждение, если чаевые не выбраны
     */
    private void tipsRadioButtonsWarning() {
        selectTipsWarning.setVisible(true);
    }

    /**
     * Проверяет, выбраны ли чаевые
     *
     * @return чаевые выбраны/не выбраны
     */
    private boolean isSelectedPercentTips() {
        return noTips.isSelected() || tips5Percent.isSelected() || tips10Percent.isSelected();
    }

    /**
     * Выводит предупреждение, если валюта не выбрана
     */
    private void currencyComboBoxWarning() {
        currencyComboBox.setValue("Вы не выбрали валюту");
    }

    /**
     * Проверяет, выбрана ли валюта
     *
     * @return Выбрана/не выбрана
     */
    private boolean isSelectedCurrency() {
        return (currencyComboBox.getSelectionModel().getSelectedIndex() != -1);
    }

    /**
     * Выводит предупреждение, если сумма чека не корректна
     */
    private void outputWarning() {
        inputBill.clear();
        inputBill.setPromptText("Введены некорректные данные");
        inputBill.setStyle("-fx-background-color: #FFAAAA;");
        clearOutputs();
    }

    /**
     * Очищает поля вывода, если сумма чека не корректна
     */
    private void clearOutputs() {
        totalRubOutput.clear();
        totalOutput.clear();
        tipsRubOutput.clear();
        tipsOutput.clear();

    }

    /**
     * Проверяет сумму чека на корректность
     *
     * @return Корректные/некорректные данные
     */
    private boolean isCorrectData() {
        return inputBill.getText().matches("^[0-9]{1,10}\\.?[0-9]{0,2}$");
    }

    /**
     * Скрывает или показывает галочку показывать в рублях, а также скрывает или показывает поля для вывода результата в рублях
     */
    public void showHiddenRubOutputs() {
        if (currencyComboBox.getSelectionModel().getSelectedIndex() != 2 & !currencyComboBox.getValue().equals("Вы не выбрали валюту")) {
            outputInRub.setVisible(true);
            if (outputInRub.isSelected() && !(currencyComboBox.getSelectionModel().getSelectedIndex() == 2))
                showHiddenRubOutputsElements(true);
            else showHiddenRubOutputsElements(false);
        } else {
            outputInRub.setVisible(false);
            showHiddenRubOutputsElements(false);
        }
    }

    /**
     * Скрывает/показывает поля вывода в рублях
     */
    private void showHiddenRubOutputsElements(boolean isVisible) {
        tipsRubLabel.setVisible(isVisible);
        tipsRubOutput.setVisible(isVisible);
        totalRubLabel.setVisible(isVisible);
        totalRubOutput.setVisible(isVisible);
    }
}